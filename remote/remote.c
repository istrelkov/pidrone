/* The MIT License (MIT)

   Copyright (c) 2014 Ivan Strelkov

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
*/
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include "SDL.h"
#include "remote.h"

#define PACKET_ELEMENTS 5
#define SERVER_PORT 8888

static SDL_Joystick* joy = NULL;

static int16_t normalize_data(int value);

int remote_init(){
   SDL_InitSubSystem(SDL_INIT_JOYSTICK);
   
   // Check for joystick
   if (SDL_NumJoysticks() < 1){
      fprintf(stderr, "No Joysticks found...\n");
      return -1;
   }
   
   joy = SDL_JoystickOpen(0);
   if (!joy) {
      fprintf(stderr, "Couldn't open Joystick 0\n");
      return -1;
   } else {
      printf("Opened Joystick 0\n");
      printf("Name: %s\n", SDL_JoystickNameForIndex(0));
      printf("Number of Axes: %d\n", SDL_JoystickNumAxes(joy));
      printf("Number of Buttons: %d\n", SDL_JoystickNumButtons(joy));
      printf("Number of Balls: %d\n", SDL_JoystickNumBalls(joy));
   }
   return 0;
}

int remote_start(char* hostname){
   uint16_t msg[PACKET_ELEMENTS];
   struct sockaddr_in server;
   int len = sizeof(struct sockaddr_in);

   struct hostent* host;
   int n, s;

   host = gethostbyname(hostname);
   if (host == NULL) {
      perror("Invalid host");
      return -1;
   }

   if(!joy){
      fprintf(stderr, "Joystick not initialized...\n");
      return -1;
   }
   
   if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
      perror("Unable to open socket");
      return -1;
   }

   memset((char *) &server, 0, sizeof(struct sockaddr_in));
   server.sin_family = AF_INET;
   server.sin_port = htons(SERVER_PORT);
   server.sin_addr = *((struct in_addr*) host->h_addr);
 
   SDL_Event event;
   uint16_t buttons = 0;

   while (1) {
      SDL_JoystickUpdate();
      while(SDL_PollEvent(&event)) {
         switch(event.type) {
            case SDL_JOYBUTTONDOWN:
               if(event.jbutton.state == SDL_PRESSED){
                  if(event.jbutton.button < 16) {
                     if(event.jbutton.button == 11){
                        remote_stop();
                        return 0;
                     } else {
                        buttons ^= 1 << event.jbutton.button;
                     }
                  }
               }
         }
      }

      msg[0] = htons(normalize_data(SDL_JoystickGetAxis(joy, 0)));
      msg[1] = htons(normalize_data(SDL_JoystickGetAxis(joy, 1)));
      msg[2] = htons(normalize_data(SDL_JoystickGetAxis(joy, 3)));
      msg[3] = htons(normalize_data(SDL_JoystickGetAxis(joy, 2)));
      msg[4] = htons(buttons);
    /*printf("%d %d %d %d %d\n", (int16_t)ntohs(msg[0]),
      (int16_t)ntohs(msg[1]), (int16_t)ntohs(msg[2]),
      (int16_t)ntohs(msg[3]), (uint16_t)ntohs(msg[4]));
      fflush(stdout);*/

      if (sendto(s, msg, PACKET_ELEMENTS*sizeof(uint16_t), 0, 
                (struct sockaddr *) &server, len) == -1) {
         perror("Unable to send data");
      }
      usleep(1000*100);
   }
}

void remote_stop(){
   if (joy && SDL_JoystickGetAttached(joy)) {
      SDL_JoystickClose(joy);
   }
}

static int16_t normalize_data(int value) {
   if(value < -32767) {
      return -32767;
   } else if (value > 32767) {
      return 32767;
   } else if ((value > -1200) && (value < 1200)) {
      return 0;
   } else {
      return value;
   }
}
