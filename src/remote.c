/* The MIT License (MIT)

   Copyright (c) 2014 Ivan Strelkov

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/time.h>
#include <pthread.h>
#include <syslog.h>
#include "remote.h"

#define FILTER_LEN 10
#define NUM_VALUES 5
#define PACKET_SIZE NUM_VALUES*sizeof(uint16_t)

#define PORT 8888

#define RX_MAX 32767
#define RX_MIN -32767
#define PITCH_RANGE 30.0F
#define ROLL_RANGE 30.0F
#define YAW_RANGE 100.0F

static int socket_fd;
static pthread_t thread;
static volatile int listening = 1;

static volatile rx_data data = {0};

void* socket_callback(void* id);
void normalize_data();

int remote_connect() {
   syslog(LOG_DEBUG, "Connecting remote...");
   pthread_attr_t attr;
   pthread_attr_init(&attr);
   pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
   if(pthread_create(&thread, &attr, socket_callback, NULL)) {
      syslog(LOG_EMERG, "Unable to create remote listener!");
      return -1;
   }
   pthread_attr_destroy(&attr);
   syslog(LOG_DEBUG, "Remote connected.");
   return 0;
}

int remote_get_data(rx_data* val) {
   val->roll = data.roll;
   val->pitch = data.pitch;
   val->yaw = data.yaw;
   val->throttle = data.throttle;
   val->flags = data.flags;
   return 0;
}

int remote_disconnect() {
   listening = 0;
   return 0;
}

void* socket_callback(void* id) {
   struct sockaddr_in self;
   socket_fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
   if(socket_fd == -1) {
      syslog(LOG_EMERG, "Unable to open UDP socket! %m");
      pthread_exit(NULL);
      return;
   }

   memset((char*) &self, 0, sizeof(struct sockaddr_in));
   self.sin_family = AF_INET;
   self.sin_port = htons(PORT);
   self.sin_addr.s_addr = htonl(INADDR_ANY);

   if(bind(socket_fd, (struct sockaddr*) &self, sizeof(self)) == -1) {
      syslog(LOG_EMERG, "Unable to bind to port %d! %m", PORT);
      pthread_exit(NULL);
      return;
   }

   fd_set socket_set;
   struct timeval timeout;
   int ready_descriptors;
   ssize_t bytes;
   struct sockaddr_in other;
   int len = sizeof(struct sockaddr_in);

   uint16_t msg[NUM_VALUES];

   while(listening) {
      FD_ZERO(&socket_set);
      FD_SET(socket_fd, &socket_set);
      timeout.tv_sec = 1;
      timeout.tv_usec = 0;

      ready_descriptors = select(socket_fd + 1, &socket_set, NULL, NULL, &timeout);
      if(ready_descriptors == -1) {
         syslog(LOG_ALERT, "UDP select error %m!");
      } else {
         if(FD_ISSET(socket_fd, &socket_set)) {
            //data ready
            bytes = recvfrom(socket_fd, msg, PACKET_SIZE, MSG_WAITALL,
                             (struct sockaddr*) &other, &len);
            if(bytes != PACKET_SIZE) {
               syslog(LOG_CRIT, "Packet size of %ld b does not match the expected %ld b!", bytes,
                      PACKET_SIZE);
            }
            normalize_data(&msg);
            fflush(stdout);
         } else {
            //timeout reached
            syslog(LOG_CRIT, "Timeout reached while waiting for input!");
            data.roll = 0.0;
            data.pitch = 0.0;
            data.yaw =  0.0;
         }
      }
   }
   close(socket_fd);
   pthread_exit(NULL);
}

void normalize_data(uint16_t* msg) {
   data.roll = (float)(int16_t)ntohs(msg[0]);
   data.pitch = (float)(int16_t)ntohs(msg[1]);
   data.yaw = (float)(int16_t)ntohs(msg[2]);
   data.throttle = (float)(int16_t)ntohs(msg[3]);
   data.flags = ntohs(msg[4]);

   data.roll = ROLL_RANGE * ((float)(data.roll - ((RX_MAX - RX_MIN) / 2) -
                                     RX_MIN) / (RX_MAX - RX_MIN));
   data.pitch = PITCH_RANGE * ((float)(data.pitch - ((RX_MAX - RX_MIN) / 2) -
                                       RX_MIN) / (RX_MAX - RX_MIN));
   data.yaw = YAW_RANGE * ((float)(data.yaw - ((RX_MAX - RX_MIN) / 2) -
                                        RX_MIN) / (RX_MAX - RX_MIN));
   data.throttle = (float)(data.throttle - RX_MIN) / (RX_MAX - RX_MIN);
}
