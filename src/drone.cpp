/* The MIT License (MIT)

   Copyright (c) 2014 Ivan Strelkov

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
*/
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <syslog.h>
extern "C" {
#include "drone.h"
#include "mpu.h"
#include "pid.h"
#include "pid_autotune.h"
#include "remote.h"
#include "esc.h"
}

#define DRONE_CONFIG_FILE "config.txt"

#define YAW_PID_KP "pid.yaw.kp"
#define YAW_PID_KI "pid.yaw.ki"
#define YAW_PID_KD "pid.yaw.kd"

#define PITCH_PID_KP "pid.pitch.kp"
#define PITCH_PID_KI "pid.pitch.ki"
#define PITCH_PID_KD "pid.pitch.kd"

#define ROLL_PID_KP "pid.roll.kp"
#define ROLL_PID_KI "pid.roll.ki"
#define ROLL_PID_KD "pid.roll.kd"

using namespace boost::property_tree;

static int fd;
static pid_control* yaw_pid;
static pid_control* pitch_pid;
static pid_control* roll_pid;
static ptree prop;
static ptree prop_defaults;

static void drone_init_defaults();

static inline void _update_esc_(float throttle, float pitch, float roll,
                                float yaw);
static inline long _nanodiff_(struct timespec before, struct timespec after);

int drone_init() {
   drone_load();

   if(mpu_connect() == -1) {
      syslog(LOG_EMERG, "Unable to connect to MPU");
      return -1;
   }
   if(remote_connect() == -1) {
      syslog(LOG_EMERG, "Unable to start remote control listener");
      return -1;
   }
   if(esc_connect() == -1) {
      syslog(LOG_EMERG, "Unable to connect to ESC controllers");
      return -1;
   }

   yaw_pid = pid_alloc(prop.get<float>(YAW_PID_KP,
                                       prop_defaults.get<float>(YAW_PID_KP)),
                       prop.get<float>(YAW_PID_KI, prop_defaults.get<float>(YAW_PID_KI)),
                       prop.get<float>(YAW_PID_KD, prop_defaults.get<float>(YAW_PID_KD)),
                       -300, 300);
   if(yaw_pid == NULL) {
      syslog(LOG_EMERG, "Unable to allocate PID controller instance");
      return -1;
   }
   pitch_pid = pid_alloc(prop.get<float>(PITCH_PID_KP,
                                         prop_defaults.get<float>(PITCH_PID_KP)),
                         prop.get<float>(PITCH_PID_KI, prop_defaults.get<float>(PITCH_PID_KI)),
                         prop.get<float>(PITCH_PID_KD, prop_defaults.get<float>(PITCH_PID_KD)),
                         -1000, 1000);
   if(pitch_pid == NULL) {
      syslog(LOG_EMERG, "Unable to allocate PID controller instance");
      return -1;
   }
   roll_pid = pid_alloc(prop.get<float>(ROLL_PID_KP,
                                        prop_defaults.get<float>(ROLL_PID_KP)),
                        prop.get<float>(ROLL_PID_KI, prop_defaults.get<float>(ROLL_PID_KI)),
                        prop.get<float>(ROLL_PID_KD, prop_defaults.get<float>(ROLL_PID_KD)),
                        -1000, 1000);
   if(roll_pid == NULL) {
      syslog(LOG_EMERG, "Unable to allocate PID controller instance");
      return -1;
   }
}

int drone_start() {
   struct timespec time = {0};
   struct timespec time_comp = {0};
   float y, p, r, dt;
   float y_o, p_o, r_o;
   rx_data remote_data;
   long time_diff;

   dt = 0.003;
   if(clock_gettime(CLOCK_MONOTONIC, &time_comp) == -1) {
      syslog(LOG_EMERG, "Unable to get current time %m");
      return -1;
   }
   while(1) {
      if(mpu_get_ypr(&y, &p, &r) == -1) {
         syslog(LOG_CRIT, "Unable to get sensor readings");
      }
      remote_get_data(&remote_data);

      if(remote_data.flags & STOP_FLAG) {
         drone_stop();
         continue;
      }

      if(clock_gettime(CLOCK_MONOTONIC, &time) == -1) {
         syslog(LOG_EMERG, "Unable to get current time %m");
      }
      if(_nanodiff_(time_comp, time) >= dt * 1000000000) {
         pid_calculate(yaw_pid, &y, &remote_data.yaw, &dt, &y_o);
         pid_calculate(pitch_pid, &p, &remote_data.pitch, &dt, &p_o);
         pid_calculate(roll_pid, &r, &remote_data.roll, &dt, &r_o);
         _update_esc_(remote_data.throttle, p_o, r_o, y_o);
         time_comp = time;
      }
   }
}

void drone_tune_roll() {
   double aTuneStep = 20, aTuneNoise = 1, aTuneStartValue = 0;
   unsigned int aTuneLookBack = 20;
   struct timespec _time = {0};
   struct timespec _time_comp = {0};
   float y, p, r, dt;
   float r_o, setp;
   long time_diff;
   rx_data remote_data;

   PID_ATune aTune(&r, &r_o);
   aTune.SetNoiseBand(aTuneNoise);
   aTune.SetOutputStep(aTuneStep);
   aTune.SetLookbackSec((int)aTuneLookBack);

   setp = 0.0;
   dt = 0.009;
   while(aTune.Runtime() == 0) {
      mpu_get_ypr(&y, &p, &r);
      remote_get_data(&remote_data);
      _update_esc_(remote_data.throttle, 0.0, r_o, 0.0);
   }
   prop.put(ROLL_PID_KP, aTune.GetKp());
   prop.put(ROLL_PID_KP, aTune.GetKi());
   prop.put(ROLL_PID_KP, aTune.GetKd());
}

void drone_tune_pitch() {
   double aTuneStep = 50, aTuneNoise = 1, aTuneStartValue = 0;
   unsigned int aTuneLookBack = 20;
   struct timespec _time = {0};
   struct timespec _time_comp = {0};
   float y, p, r, dt;
   float p_o, setp;
   long time_diff;
   rx_data remote_data;

   PID_ATune aTune(&p, &p_o);
   aTune.SetNoiseBand(aTuneNoise);
   aTune.SetOutputStep(aTuneStep);
   aTune.SetLookbackSec((int)aTuneLookBack);

   setp = 0.0;
   dt = 0.009;
   while(aTune.Runtime() == 0) {
      mpu_get_ypr(&y, &p, &r);
      remote_get_data(&remote_data);
      _update_esc_(remote_data.throttle, p_o, 0.0, 0.0);
   }
   prop.put(PITCH_PID_KP, aTune.GetKp());
   prop.put(PITCH_PID_KI, aTune.GetKi());
   prop.put(PITCH_PID_KD, aTune.GetKd());
}

int drone_stop() {
   _update_esc_(0.0, 0.0, 0.0, 0.0);
}

int drone_close() {
   esc_disconnect();
   mpu_close();
   pid_free(yaw_pid);
   pid_free(pitch_pid);
   pid_free(roll_pid);
   remote_disconnect();
   drone_save();
}

void drone_load() {
   drone_init_defaults();
   try {
      info_parser::read_info(DRONE_CONFIG_FILE, prop, prop_defaults);
   } catch(std::exception const&  ex) {
      syslog(LOG_WARNING, "Unable to load settings. %s", ex.what());
   }
}

void drone_restore(){
   drone_init_defaults();
   info_parser::write_info(DRONE_CONFIG_FILE, prop_defaults);
}

void drone_save() {
   info_parser::write_info(DRONE_CONFIG_FILE, prop);
}

void drone_init_defaults(){
   prop_defaults.put(YAW_PID_KP, 2.0);
   prop_defaults.put(YAW_PID_KI, 0.0);
   prop_defaults.put(YAW_PID_KD, 0.0);

   prop_defaults.put(PITCH_PID_KP, 0.84);
   prop_defaults.put(PITCH_PID_KI, 1.39);
   prop_defaults.put(PITCH_PID_KD, 0.4);

   prop_defaults.put(ROLL_PID_KP, 0.84);
   prop_defaults.put(ROLL_PID_KI, 1.39);
   prop_defaults.put(ROLL_PID_KD, 0.4);
}

static inline void _update_esc_(float throttle, float pitch, float roll,
                                float yaw) {
   if(esc_update(FRONT, (throttle * 600) + yaw / 2 + pitch / 2) == -1) {
      syslog(LOG_EMERG, "Unable to access front ESC");
   }
   if(esc_update(BACK, (throttle * 600) + yaw / 2 - pitch / 2) == -1) {
      syslog(LOG_EMERG, "Unable to access back ESC");
   }
   if(esc_update(LEFT, (throttle * 600) - yaw / 2 + roll / 2) == -1) {
      syslog(LOG_EMERG, "Unable to access left ESC");
   }
   if(esc_update(RIGHT, (throttle * 600) - yaw / 2 - roll / 2) == -1) {
      syslog(LOG_EMERG, "Unable to access right ESC");
   }
}

static inline long _nanodiff_(struct timespec before, struct timespec after) {
   return (after.tv_sec * 1000000000 + after.tv_nsec) - (before.tv_sec * 1000000000
          + before.tv_nsec);
}
