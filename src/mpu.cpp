/* The MIT License (MIT)

   Copyright (c) 2014 Ivan Strelkov

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <syslog.h>
#include "I2Cdev.h"
#include "mpu.h"
#include "MPU6050_6Axis_MotionApps20.h"

bool dmpReady = false;
uint8_t mpuIntStatus;
uint8_t devStatus;
uint16_t packetSize;
uint16_t fifoCount;
uint8_t fifoBuffer[64];

Quaternion q;
VectorFloat gravity;
float ypr[3];

MPU6050 mpu;

int mpu_connect() {
   syslog(LOG_INFO, "Initializing I2C devices...");
   mpu.initialize();

   syslog(LOG_DEBUG, "Testing device connections...");

   if(mpu.testConnection()) {
      syslog(LOG_DEBUG, "MPU6050 connection successful");
   } else {
      syslog(LOG_EMERG, "MPU6050 connection failed");
      return -1;
   }

   syslog(LOG_DEBUG, "Initializing DMP...");
   devStatus = mpu.dmpInitialize();

   if(devStatus == 0) {
      syslog(LOG_DEBUG, "Enabling DMP...");
      mpu.setDMPEnabled(true);

      mpuIntStatus = mpu.getIntStatus();

      syslog(LOG_DEBUG, "DMP ready!");
      dmpReady = true;

      packetSize = mpu.dmpGetFIFOPacketSize();
   } else {
      // 1 = initial memory load failed
      // 2 = DMP configuration updates failed
      // (if it's going to break, usually the code will be 1)
      syslog(LOG_EMERG, "DMP Initialization failed (code %d)", devStatus);
      return -1;
   }
}

int mpu_get_ypr(float* yaw, float* pitch, float* roll) {
   fifoCount = mpu.getFIFOCount();

   if(fifoCount == 1024) {
      mpu.resetFIFO();

      syslog(LOG_ALERT, "FIFO overflow!");
      return -1;
      // check for DMP data ready interrupt
   } else if(fifoCount >= 42) {
      mpu.getFIFOBytes(fifoBuffer, packetSize);

      mpu.dmpGetQuaternion(&q, fifoBuffer);
      mpu.dmpGetGravity(&gravity, &q);
      mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
      *yaw = ypr[0] * 180 / M_PI;
      *pitch = ypr[1] * 180 / M_PI;
      *roll = ypr[2] * 180 / M_PI;
      //printf("ypr  %7.2f %7.2f %7.2f    \n", ypr[0] * 180/M_PI, ypr[1] * 180/M_PI, ypr[2] * 180/M_PI);
   }
   return 0;
}

int mpu_close() {
   return 0;
}
