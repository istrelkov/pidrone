/* The MIT License (MIT)

   Copyright (c) 2014 Ivan Strelkov

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
*/
#include <stdlib.h>
#include <syslog.h>
#include "esc.h"
#include "pwm.h"

// 1us increment
#define ESC_PULSE_WIDTH_INCREMENT_GRANULARITY_US 1
#define ESC_MEM_CHANNEL 0
// 3ms carrier
#define ESC_SUBCYCLE_TIME_US 3000

#define ESC_INITIAL_PULSE_WIDTH 1000
#define ESC_MIN_PULSE_WIDTH 1010
#define ESC_MAX_PULSE_WIDTH 1700

#define ESC_FRONT 23
#define ESC_BACK 17
#define ESC_LEFT 18
#define ESC_RIGHT 22

static int resolve_esc_address(esc_position position) {
   switch(position) {
   case FRONT:
      return ESC_FRONT;
   case BACK:
      return ESC_BACK;
   case LEFT:
      return ESC_LEFT;
   case RIGHT:
      return ESC_RIGHT;
   default:
      return -1;
   }
}

int esc_connect() {
   // Setting PWMlib log level
   set_loglevel(LOG_LEVEL_ERRORS);

   syslog(LOG_DEBUG, "Setting up ESC with pulse granularity %d!",
          ESC_PULSE_WIDTH_INCREMENT_GRANULARITY_US);
   if(setup(ESC_PULSE_WIDTH_INCREMENT_GRANULARITY_US,
            DELAY_VIA_PWM) != EXIT_SUCCESS) {
      syslog(LOG_EMERG, "Unable to setup ESC PWM!");
      return -1;
   }

   syslog(LOG_DEBUG, "Initializing ESC channel %d with cycle time %d!",
          ESC_MEM_CHANNEL, ESC_SUBCYCLE_TIME_US);
   if(init_channel(ESC_MEM_CHANNEL, ESC_SUBCYCLE_TIME_US) != EXIT_SUCCESS) {
      syslog(LOG_EMERG, "Unable to initialize ESC PWM channel!");
      return -1;
   }

   if(add_channel_pulse(ESC_MEM_CHANNEL, ESC_FRONT, 0,
                        ESC_INITIAL_PULSE_WIDTH) != EXIT_SUCCESS) {
      syslog(LOG_EMERG, "Unable to add pulse to GPIO %d!", ESC_FRONT);
      return -1;
   }
   if(add_channel_pulse(ESC_MEM_CHANNEL, ESC_BACK, 0,
                        ESC_INITIAL_PULSE_WIDTH) != EXIT_SUCCESS) {
      syslog(LOG_EMERG, "Unable to add pulse to GPIO %d!", ESC_BACK);
      return -1;
   }
   if(add_channel_pulse(ESC_MEM_CHANNEL, ESC_LEFT, 0,
                        ESC_INITIAL_PULSE_WIDTH) != EXIT_SUCCESS) {
      syslog(LOG_EMERG, "Unable to add pulse to GPIO %d!", ESC_LEFT);
      return -1;
   }
   if(add_channel_pulse(ESC_MEM_CHANNEL, ESC_RIGHT, 0,
                        ESC_INITIAL_PULSE_WIDTH) != EXIT_SUCCESS) {
      syslog(LOG_EMERG, "Unable to add pulse to GPIO %d!", ESC_RIGHT);
      return -1;
   }

   syslog(LOG_INFO, "ESC successfully setup!");
   return 0;
}


int esc_update(esc_position position, int value) {
   int pulse_width = ESC_MIN_PULSE_WIDTH + value;
   int address = resolve_esc_address(position);

   if(address == -1) {
      return -1;
   }

   if(pulse_width > ESC_MAX_PULSE_WIDTH) {
      pulse_width = ESC_MAX_PULSE_WIDTH;
   } else if(pulse_width < ESC_MIN_PULSE_WIDTH) {
      pulse_width = ESC_MIN_PULSE_WIDTH;
   }

   return add_channel_pulse(ESC_MEM_CHANNEL, address, 0, pulse_width);
}

int esc_disconnect() {
   syslog(LOG_INFO, "ESC shutdown!");
   shutdown();
   syslog(LOG_DEBUG, "ESC shutdown successful!");
   return 0;
}
