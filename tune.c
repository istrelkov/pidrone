/* The MIT License (MIT)

   Copyright (c) 2014 Ivan Strelkov

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
*/
#include <stdlib.h>
#include <syslog.h>
#include <stdio.h>
#include "drone.h"

int main(int argc, const char* argv[]) {
   if(argc < 2) {
      fprintf(stderr, "Usage: %s [[restore, roll, pitch] <axis>]\n", argv[0]);
      return EXIT_FAILURE;
   }

   setlogmask(LOG_UPTO(LOG_DEBUG));
   openlog("pidrone-tune", LOG_PID | LOG_CONS | LOG_NDELAY , LOG_USER);

   if(strcmp(argv[1], "roll") == 0) {
      if(drone_init() == -1) {
         return EXIT_FAILURE;
      }
      drone_tune_roll();
      drone_close();
   } else if(strcmp(argv[1], "pitch") == 0) {
      if(drone_init() == -1) {
         return EXIT_FAILURE;
      }
      drone_tune_pitch();
      drone_close();
   } else if(strcmp(argv[1], "restore") == 0) {
      drone_restore();
   } else {
      fprintf(stderr, "Invalid argument.\nUsage: %s [[restore, roll, pitch] <module>]\n",
              argv[0]);
      closelog();
      return EXIT_FAILURE;
   }

   closelog();
   return EXIT_SUCCESS;
}
