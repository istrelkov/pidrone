#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>

#define BUF_SIZE 1024

int main(int argc, char *argv[]) {
    uint16_t msg[BUF_SIZE];
    struct sockaddr_in server;
    int len = sizeof(struct sockaddr_in);

    struct hostent *host;
    int n, s, port;

    if (argc < 3) {
    	fprintf(stderr, "Usage: %s <host> <port> <message>\n", argv[0]);
	    return 1;
    }

    host = gethostbyname(argv[1]);
    if (host == NULL) {
	    perror("gethostbyname");
    	return 1;
    }

    port = atoi(argv[2]);

    /* initialize socket */
    if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
	    perror("socket");
	    return 1;
    }

    /* initialize server addr */
    memset((char *) &server, 0, sizeof(struct sockaddr_in));
    server.sin_family = AF_INET;
    server.sin_port = htons(port);
    server.sin_addr = *((struct in_addr*) host->h_addr);

    msg[0] =  htons(32767);
    msg[1] =  htons(-32767);
    printf("Sending data to %s:%s\n", argv[1], argv[2]);
    /*
    Could also be written into char* buff by:
    uint16_t nw_int = htons(val);
    buf[size++] = nw_int >> 8;
    buf[size++] = nw_int;
    */
    if (sendto(s, msg, 2*sizeof(uint16_t), 0, (struct sockaddr *) &server, len) == -1) {
	    perror("sendto()");
	    return 1;
    }

    close(s);
    return 0;
}
