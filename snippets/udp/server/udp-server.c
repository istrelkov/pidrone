#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/time.h>

#define BUF_SIZE 1024

int main(int argc, char* argv[]) {
    char buf[BUF_SIZE];
    uint16_t msg[BUF_SIZE];
    struct sockaddr_in self, other;
    int len = sizeof(struct sockaddr_in);
    int n, s, port;

    if (argc < 2) {
    	fprintf(stderr, "Usage: %s <port>\n", argv[0]);
	    return 1;
    }

    /* initialize socket */
    if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
	    perror("socket");
    	return 1;
    }

    /* bind to server port */
    port = atoi(argv[1]);
    memset((char *) &self, 0, sizeof(struct sockaddr_in));
    self.sin_family = AF_INET;
    self.sin_port = htons(port);
    self.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(s, (struct sockaddr *) &self, sizeof(self)) == -1) {
	    perror("bind");
    	return 1;
    }

    fd_set rfds;
    struct timeval tv;
    int retval;

    while(1) {
        FD_ZERO(&rfds);
        FD_SET(s, &rfds);
        tv.tv_sec = 5;
        tv.tv_usec = 0;

        retval = select(s+1, &rfds, NULL, NULL, &tv);

        if (retval == -1)
            perror("select()");
        if(FD_ISSET(s, &rfds)){
            printf("Data is available now.\n");
            /*
            Could also be read into char* buff by:
            int16_t result = ntohs((((uint16_t)buf[0]) << 8) | (uint16_t)buf[1]);
            */
            n = recvfrom(s, msg, BUF_SIZE, 0, (struct sockaddr *) &other, &len);
            printf("Received from %s:%d: ",
    		inet_ntoa(other.sin_addr),
	    	ntohs(other.sin_port));
            printf("Bytes %d, %ld uints Data: ", n, n/sizeof(uint16_t));
            int i;
            for(i=0; i<n/sizeof(uint16_t); i++){
                printf("%d ", (int16_t)ntohs(msg[i]));
            }
            printf("\n");
    	    fflush(stdout);
        } else {
            printf("No data within five seconds.\n");
        }
    }

    close(s);
    return 0;
}
