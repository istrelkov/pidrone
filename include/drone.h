/* The MIT License (MIT)

   Copyright (c) 2014 Ivan Strelkov

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
*/
#ifndef DRONE_H
#define DRONE_H

#define STOP_FLAG (1 << 10)

#ifdef __cplusplus
extern "C" {
#endif // ifdef __cplusplus
int drone_init();
int drone_start();
void drone_load();
void drone_restore();
void drone_save();
void drone_tune_roll();
void drone_tune_pitch();
int drone_stop();
int drone_close();
#ifdef __cplusplus
} // extern "C"
#endif // ifdef __cplusplus

#endif /* ifndef DRONE_H */
