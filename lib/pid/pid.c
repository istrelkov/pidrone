/* The MIT License (MIT)

   Copyright (c) 2014 Ivan Strelkov

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
*/
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include "pid.h"

inline void constrain(pid_control* pid, float* value);

pid_control* pid_alloc(float kp, float ki, float kd, float min, float max) {
   pid_control* result = calloc(1, sizeof(pid_control));
   if(result == NULL) {
      return NULL;
   }
   result -> kp = kp;
   result -> ki = ki;
   result -> kd = kd;
   result -> min = min;
   result -> max = max;
   result -> previous_error = 0.0;
   result -> integral = 0.0;
   return result;
}

void pid_calculate(pid_control* pid, float* position, float* setpoint,
                   float* dt,
                   float* output) {
   float error;
   float derivative;

   error = *setpoint - *position;
   pid -> integral += error * (*dt);

   constrain(pid, &(pid -> integral));

   derivative = (error - pid -> previous_error) / (*dt);
   *output = (pid -> kp) * error + (pid -> ki) * (pid -> integral) +
             (pid -> kd) * derivative;
   pid -> previous_error = error;
   constrain(pid, output);
}

void pid_set_tune(pid_control* pid, float kp, float ki, float kd) {
   pid -> kp = kp;
   pid -> ki = ki;
   pid -> kd = kd;
}

void pid_free(pid_control* pid) {
   if(pid != NULL) {
      free(pid);
   }
}

inline void constrain(pid_control* pid, float* value) {
   if(*value > pid -> max) {
      *value = pid -> max;
   } else if(*value < pid -> min) {
      *value = pid -> min;
   }
}
