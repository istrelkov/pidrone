/* The MIT License (MIT)

   Copyright (c) 2014 Ivan Strelkov

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <syslog.h>
#include "mpu.h"
#include "esc.h"
#include "remote.h"
#include "pid.h"
#include "drone.h"

void test_mpu() {
   float y, p, r;
   if(mpu_connect()) {
      while(1) {
         mpu_get_ypr(&y, &p, &r);
         printf("%f, %f, %f", y, p, r);
      }
   }
}

void test_esc() {
   esc_connect();
   sleep(3);
   esc_update(FRONT, 70);
   sleep(3);
   esc_update(FRONT, 0);
   esc_update(BACK, 70);
   sleep(3);
   esc_update(BACK, 0);
   esc_update(LEFT, 70);
   sleep(3);
   esc_update(LEFT, 0);
   esc_update(RIGHT, 70);
   sleep(3);
   esc_update(RIGHT, 0);
   esc_disconnect();
}

void test_remote() {
   rx_data data;
   int i;

   remote_connect();

   while(i < 1000) {
      remote_get_data(&data);
      printf("remote: %f %f %f %f %d\n", data.roll, data.pitch, data.yaw,
             data.throttle, data.flags);
      i++;
      usleep(1000 * 100);
   }
}

int main(int argc, const char* argv[]) {
   if(argc < 2) {
      fprintf(stderr, "Usage: %s [[mpu, esc, remote] <module>]\n", argv[0]);
      return EXIT_FAILURE;
   }

   setlogmask(LOG_UPTO(LOG_DEBUG));
   openlog("pidrone-test", LOG_PID | LOG_CONS | LOG_NDELAY , LOG_USER);

   if(strcmp(argv[1], "mpu") == 0) {
      test_mpu();
   } else if(strcmp(argv[1], "esc") == 0) {
      test_esc();
   } else if(strcmp(argv[1], "remote") == 0) {
      test_remote();
   } else {
      fprintf(stderr, "Invalid argument.\nUsage: %s [[mpu, esc, remote] <module>]\n",
              argv[0]);
      closelog();
      return EXIT_FAILURE;
   }

   closelog();
   return EXIT_SUCCESS;
}
